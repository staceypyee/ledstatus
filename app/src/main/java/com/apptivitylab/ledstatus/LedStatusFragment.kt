package com.apptivitylab.ledstatus


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import kotlinx.android.synthetic.main.fragment_led_status.*

class LedStatusFragment : Fragment(), View.OnClickListener {

    companion object {

        fun newInstance(): LedStatusFragment {
            val fragment = LedStatusFragment()

            val args = Bundle()
            fragment.arguments = args

            return fragment
        }
    }

    enum class LedStatus {
        OFF, BLINKING, ON;

        fun statusRes(): Int {
            when (this) {
                LedStatusFragment.LedStatus.OFF -> return R.string.led_off
                LedStatusFragment.LedStatus.BLINKING -> return R.string.led_blinking
                LedStatusFragment.LedStatus.ON -> return R.string.led_on
            }
        }
    }

    private val firstLedTypeImageButtons = ArrayList<ImageButton>()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater!!.inflate(R.layout.fragment_led_status, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.firstLedTypeImageButtons.addAll(listOf(
                this.ledOffImageButton,
                this.ledBlinkingImageButton,
                this.ledOnImageButton
        ))

        this.firstLedTypeImageButtons.forEach { button ->
            button.isSelected = false
            button.setOnClickListener(this)
        }
        this.firstLedTypeImageButtons.first().isSelected = true
    }

    override fun onClick(v: View?) {
        (v as? ImageButton)?. let {
            val selectedIndex = this.firstLedTypeImageButtons.indexOf(v)
            this.firstLedTypeImageButtons.forEachIndexed { index, imageButton ->
                imageButton.isSelected = index == selectedIndex

                if (imageButton.isSelected) {
                    this.ledStatusTextView.text = getString(LedStatus.values()[selectedIndex].statusRes())
                }
            }
        }
    }
}
