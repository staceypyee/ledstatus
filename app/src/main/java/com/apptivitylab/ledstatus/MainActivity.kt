package com.apptivitylab.ledstatus

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            this.supportFragmentManager.beginTransaction()
                    .add(this.contentContainer.id,
                            LedStatusFragment.newInstance()
                    )
                    .commit()
        }
    }
}
